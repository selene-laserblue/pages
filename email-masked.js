// Some bots evaluate JS and then snapshot the DOM,
// so we just wait for 500ms.

const email = document.querySelector("#email-masked");
email.style.color = "gray";

setTimeout(() => {
	const part1 = "selene";
	const part2 = "laserblue";
	const part3 = "proton.me";

	email.innerHTML = "";
	email.style.color = null;
	const a = document.createElement("a");
	a.href = `mailto:${part1}.${part2}@${part3}`;
	a.textContent = `${part1}.${part2}@${part3}`;
	email.appendChild(a);
}, 500);
